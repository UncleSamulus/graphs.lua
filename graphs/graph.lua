-- Graphs utilities
-- Copyright (C) 2023 Samuel ORTION

-- Convert an adjacency list to an adjacency matrix
-- The adjacency list is a table of tables, where each table is a vertex whose 
-- first element is the vertex number and the other elements are tables of the form {to, weight}
local function adjacency_matrix(adjacency_list)
    local matrix = {}
    local n = #adjacency_list
    -- Initialize the matrix
    for i = 1,n do
        matrix[i] = {}
        for j = 1,n do
            matrix[i][j] = 0
        end
    end
    -- Fill the matrix
    for i = 1,n do
        for j = 2,#adjacency_list[i] do
            print("i = " .. i .. ", j = " .. j)
            from = adjacency_list[i][1]
            to = adjacency_list[i][j][1]
            weight = adjacency_list[i][j][2]
            matrix[from][to] = weight
        end
    end
    return matrix
end

-- Convert an adjacency matrix to an adjacency list
local function adjacency_list(adjacency_matrix)
    local list = {}
    local n = #adjacency_matrix
    -- Initialize the list
    for i = 1,n do
        list[i] = {}
    end
    -- Fill the list
    for i = 1,n do
        for j = 1,n do
            if adjacency_matrix[i][j] == 1 then
                table.insert(list[i], j)
            end
        end
    end
    return list
end


local function list2weights_matrix(weighted_adjacency_list)
    -- Get all the vertices
    local vertices = {}
    for i = 1,#weighted_adjacency_list do
        if not vertices[weighted_adjacency_list[i][1]] then
            vertices[weighted_adjacency_list[i][1]] = true
        end
        for j = 2,#weighted_adjacency_list[i] do
            if not vertices[weighted_adjacency_list[i][j][1]] then
                vertices[weighted_adjacency_list[i][j][1]] = true
            end
        end
    end
    -- Initialize the matrix
    local matrix = {}
    for i = 1,#vertices do
        matrix[i] = {}
        for j = 1,#vertices do
            if i == j then
                matrix[i][j] = 0
            else
                matrix[i][j] = math.huge
            end
        end
    end
    -- Fill the matrix
    for i = 1,#weighted_adjacency_list do
        local from = weighted_adjacency_list[i][1]
        for j = 2,#weighted_adjacency_list[i] do
            local to = weighted_adjacency_list[i][j][1]
            local weight = weighted_adjacency_list[i][j][2]
            matrix[from][to] = weight
        end
    end
    return matrix
end

return {
    adjacency_matrix = adjacency_matrix,
    adjacency_list = adjacency_list,
    list2weights_matrix = list2weights_matrix
}