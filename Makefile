install:
	luarocks make --local

setup:
	luarocks path --bin | bash

test:
	lua ./tests/test_graphs.lua