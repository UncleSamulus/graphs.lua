# graphs.lua

An attempt to implement Graphs algorithms in Lua.

>A little piece of work in context of a course at Évry - Paris-Saclay University : *Algorithmes des graphes* given by Lélia BLIN for third year of computer science bachelor.

***

## Description

This project is an attempt to implement classic graph algorithms in Lua.

Hopefully this could lead to a usable package for LuaLaTeX.

<!-- ## Badges -->

<img width=250 src="https://upload.wikimedia.org/wikipedia/commons/5/5b/Tree_of_life_with_genome_size.svg" alt="Tree of life via Wikimedia Commons, Public domain, Gringer." title="An example of relatively large graph: the tree of life">

## Installation

### Requirements

- [Lua](https://www.lua.org/)
- [LuaRocks](https://luarocks.org/)

### Setup

```bash
luarocks make --local
```

If it was not already done: add the proper folder to your `PATH` environment variable:
```bash
luarocks path --bin
```

### Tests

```bash
luarocks test
```

## Usage

Some examples are written in the [./examples](./example) folder.

## Support

Please use the issue tracker for remarks, suggestion or questions.

## Roadmap

No release planned (for now).

## Contributing

Any contributions to this little project is, as always, welcome!

## Authors and acknowledgment

I thank my professor for teaching me some of the classical graphs algorithms.

## License

[MIT](./LICENSE)

## Project status

Developped for fun, in an afternoon (sort of).