-- Example graphs for testing
package.path = './graphs/?.lua;' .. package.path

local lib = require "graphs"

-- Test
local adjacency_list = {
    {1, {3, 3}},
    {2, {1, -4}},
    {3, {4, 2}},
    {4, {6, -5}},
    {5, {3, 7}, {6, -2}, {1, -1}},
    {6, {2, 8}, {3, 5}, {4, 10}}
}

local weights_matrix = lib.graph.adjacency_matrix(adjacency_list)

lib.print.print_matrix(weights_matrix)
